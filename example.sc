load-library "/usr/lib/libglfw.so"
load-library "/usr/lib/libvulkan.so"

using import utils

vvv bind helpers
vvv include
""""
    #include <assert.h>
    #include <string.h>
    #define GLFW_INCLUDE_VULKAN
    #include <GLFW/glfw3.h>
    #include <stdbool.h>
    #include <stdio.h>

    #define vkCheck(x) if ((x) != VK_SUCCESS) fprintf(stderr, "Failure while executing Vulkan command at line %d\n", __LINE__)

    bool getMemoryTypeFromProperties(VkPhysicalDeviceMemoryProperties memProps, uint32_t typeBits, VkFlags requirementsMask, uint32_t* typeIndex)
    {
        for (uint32_t i = 0; i < VK_MAX_MEMORY_TYPES; ++i) {
            if ((typeBits & 1) == 1) {
                if ((memProps.memoryTypes[i].propertyFlags & requirementsMask) == requirementsMask) {
                    *typeIndex = i;
                    return true;
                }
            }
            typeBits >>= 1;
        }
        return false;
    }

    void setImageLayout(VkCommandBuffer* cmdBuf, VkDevice device, VkCommandPool cmdPool, VkImage image, VkImageAspectFlags aspectMask, VkImageLayout oldImageLayout, VkImageLayout newImageLayout, VkAccessFlagBits srcAccessMask)
    {
        // allocate a command buffer if we don't already have one
        if (*cmdBuf == VK_NULL_HANDLE) {
            VkCommandBufferAllocateInfo cmdBufAllocInfo = (VkCommandBufferAllocateInfo){
                .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
                .pNext = NULL,
                .commandPool = cmdPool,
                .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                .commandBufferCount = 1,
            };

            vkCheck(vkAllocateCommandBuffers(device, &cmdBufAllocInfo, cmdBuf));

            VkCommandBufferInheritanceInfo cmdBufInhInfo = (VkCommandBufferInheritanceInfo){
                .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO,
                .pNext = NULL,
                .renderPass = VK_NULL_HANDLE,
                .subpass = 0,
                .framebuffer = VK_NULL_HANDLE,
                .occlusionQueryEnable = VK_FALSE,
                .queryFlags = 0,
                .pipelineStatistics = 0,
            };

            VkCommandBufferBeginInfo cmdBufBeginInfo = (VkCommandBufferBeginInfo){
                .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
                .pNext = NULL,
                .flags = 0,
                .pInheritanceInfo = &cmdBufInhInfo,
            };
            vkCheck(vkBeginCommandBuffer(*cmdBuf, &cmdBufBeginInfo));
        }

        VkImageMemoryBarrier imageMemBarrier = (VkImageMemoryBarrier){
            .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            .pNext = NULL,
            .srcAccessMask = srcAccessMask,
            .dstAccessMask = 0,
            .oldLayout = oldImageLayout,
            .newLayout = newImageLayout,
            .image = image,
            .subresourceRange = (VkImageSubresourceRange){ aspectMask, 0, 1, 0, 1 },
        };

        if (newImageLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
            imageMemBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        } else if (newImageLayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL) {
            imageMemBarrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        } else if (newImageLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
            imageMemBarrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
        } else if (newImageLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
            imageMemBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_INPUT_ATTACHMENT_READ_BIT;
        }

        VkPipelineStageFlags srcStages = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        VkPipelineStageFlags destStages = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;

        vkCmdPipelineBarrier(*cmdBuf, srcStages, destStages, 0, 0, NULL, 0, NULL, 1, &imageMemBarrier);
    }

    static bool checkInstanceLayers(uint32_t checkCount, const char* const* checkNames, uint32_t layerCount, VkLayerProperties* layers)
    {
        for (uint32_t i = 0; i < checkCount; ++i) {
            bool found = false;
            for (uint32_t j = 0; j < layerCount; ++j) {
                if (strcmp(checkNames[i], layers[j].layerName) == 0) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                fprintf(stderr, "Cannot find instance layer %s\n", checkNames[i]);
                return false;
            }
        }
        return true;
    }

    static const bool validate = false;

    VkBool32 debugCallback(VkFlags msgFlags, VkDebugReportObjectTypeEXT objType, uint64_t srcObject, size_t location, int32_t msgCode, const char* pLayerPrefix, const char* pMsg, void* pUserData) {
        if (msgFlags & VK_DEBUG_REPORT_ERROR_BIT_EXT)
        {
            fprintf(stderr, "ERROR: [%s] Code %d : %s\n", pLayerPrefix, msgCode, pMsg);
        } else if (msgFlags & VK_DEBUG_REPORT_WARNING_BIT_EXT)
        {
            fprintf(stderr, "WARNING: [%s] Code %d : %s\n", pLayerPrefix, msgCode, pMsg);
        } else {
            fprintf(stderr, "INFO: [%s] Code %d : %s\n", pLayerPrefix, msgCode, pMsg);
        }
        return VK_FALSE;
    }

    void buildCommandBuffers(uint32_t i, uint32_t* width, uint32_t* height, VkCommandBuffer* cmdBufs, VkCommandBufferBeginInfo* cmdBufBeginInfo, VkImage* swapchainImages, VkFramebuffer* framebuffers, VkRenderPass renderPass, VkClearValue* clearValues) {
        vkCheck(vkBeginCommandBuffer(cmdBufs[i], cmdBufBeginInfo));

        VkImageMemoryBarrier imageMemBarrier = {
            .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            .pNext = NULL,
            .srcAccessMask = 0,
            .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
            .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
            .newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
            .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
            .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
            .image = swapchainImages[i],
            .subresourceRange = (VkImageSubresourceRange){ VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 },
        };

        vkCmdPipelineBarrier(cmdBufs[i], VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, NULL, 0, NULL, 1, &imageMemBarrier);

        VkRenderPassBeginInfo rpBeginInfo = {
            .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
            .pNext = NULL,
            .renderPass = renderPass,
            .framebuffer = framebuffers[i],
            .renderArea.offset.x = 0,
            .renderArea.offset.y = 0,
            .renderArea.extent.width = *width,
            .renderArea.extent.height = *height,
            .clearValueCount = 2,
            .pClearValues = clearValues,
        };

        vkCmdBeginRenderPass(cmdBufs[i], &rpBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
        VkViewport viewport = {
            .width = (float)(*width),
            .height = (float)(*height),
            .minDepth = 0.0f,
            .maxDepth = 1.0f,
        };
        vkCmdSetViewport(cmdBufs[i], 0, 1, &viewport);

        VkRect2D scissor = {
            .extent = { .width = *width, .height = *height },
            .offset.x = 0,
            .offset.y = 0,
        };
        vkCmdSetScissor(cmdBufs[i], 0, 1, &scissor);
        vkCmdEndRenderPass(cmdBufs[i]);

        VkImageMemoryBarrier prePresentBarrier = {
            .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            .pNext = NULL,
            .srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
            .dstAccessMask = VK_ACCESS_MEMORY_READ_BIT,
            .oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
            .newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
            .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
            .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
            .subresourceRange = (VkImageSubresourceRange){ VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 },
            .image = swapchainImages[i],
        };

        vkCmdPipelineBarrier(cmdBufs[i], VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, NULL, 0, NULL, 1, &prePresentBarrier);
        vkCheck(vkEndCommandBuffer(cmdBufs[i]));
    }

    int execute(uint32_t* width, uint32_t* height, GLFWwindow* window, VkDevice device, VkQueue queue, VkSwapchainKHR swapchain, VkCommandPool cmdPool, VkImage* swapchainImages, uint32_t currentSwapchainImage, VkCommandBuffer cmdBuf, VkImageView* presentViews, VkRenderPass renderPass, VkFramebuffer* framebuffers, VkCommandBuffer* cmdBufs, VkCommandBufferBeginInfo* cmdBufBeginInfo, VkClearValue* clearValues)
    {
        // build our drawing command buffers
        for (uint32_t i = 0; i < 2; ++i) buildCommandBuffers(i, width, height, cmdBufs, cmdBufBeginInfo, swapchainImages, framebuffers, renderPass, clearValues);

        // main loop
        while (!glfwWindowShouldClose(window)) {
            glfwPollEvents();

            // do the rendering
            VkSemaphore presentCompleteSemaphore;
            VkSemaphoreCreateInfo pcsCreateInfo = {
                .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
                .pNext = NULL,
                .flags = 0,
            };

            vkCheck(vkCreateSemaphore(device, &pcsCreateInfo, NULL, &presentCompleteSemaphore));

            VkResult res = vkAcquireNextImageKHR(device, swapchain, UINT64_MAX, presentCompleteSemaphore, VK_NULL_HANDLE, &currentSwapchainImage);
            if (res == VK_ERROR_OUT_OF_DATE_KHR) {
                assert(false && "Swapchain is out-of-date!");
            } else if (res == VK_SUBOPTIMAL_KHR) {
            } else {
                assert(res == VK_SUCCESS);
            }

            VkPipelineStageFlags pipelineStageFlags = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
            VkSubmitInfo submitInfo = {
                .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
                .pNext = NULL,
                .waitSemaphoreCount = 1,
                .pWaitSemaphores = &presentCompleteSemaphore,
                .pWaitDstStageMask = &pipelineStageFlags,
                .commandBufferCount = 1,
                .pCommandBuffers = &cmdBufs[currentSwapchainImage],
                .signalSemaphoreCount = 0,
                .pSignalSemaphores = NULL,
            };

            res = vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);
            assert(res == VK_SUCCESS);

            VkPresentInfoKHR present = {
                .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
                .pNext = NULL,
                .swapchainCount = 1,
                .pSwapchains = &swapchain,
                .pImageIndices = &currentSwapchainImage,
            };

            res = vkQueuePresentKHR(queue, &present);
            if (res == VK_ERROR_OUT_OF_DATE_KHR) {
                assert(false && "Swapchain out of date!");
            } else if (res == VK_SUBOPTIMAL_KHR) {
            } else {
                assert(res == VK_SUCCESS);
            }

            res = vkQueueWaitIdle(queue);
            assert(res == VK_SUCCESS);

            vkDestroySemaphore(device, presentCompleteSemaphore, NULL);
        }

        vkFreeCommandBuffers(device, cmdPool, 2, cmdBufs);

        for (uint32_t i = 0; i < 2; ++i) {
            vkDestroyImageView(device, presentViews[i], NULL);
            vkDestroyFramebuffer(device, framebuffers[i], NULL);
        }

        return 0;
    }

run-stage;

helpers.extern.glfwInit;
defer helpers.extern.glfwTerminate
assert (helpers.extern.glfwVulkanSupported);

local width : u32 = 1280
local height : u32 = 720

helpers.extern.glfwWindowHint helpers.define.GLFW_CLIENT_API helpers.define.GLFW_NO_API
helpers.extern.glfwWindowHint helpers.define.GLFW_RESIZABLE helpers.define.GLFW_FALSE
let window =
    helpers.extern.glfwCreateWindow (width as i32) (height as i32) "Vulkan on GLFW" null null
defer helpers.extern.glfwDestroyWindow window

local instanceExtCount : u32
let reqInstanceExts = (helpers.extern.glfwGetRequiredInstanceExtensions &instanceExtCount)

if (not null == reqInstanceExts)
    print "Required instance extensions:"
    for i in (range instanceExtCount)
        print
            string
                reqInstanceExts @ i

inline MAKE_VERSION (major minor patch)
    (major << 22) | (minor << 12) | patch

local appInfo : helpers.struct.VkApplicationInfo
    sType = helpers.enum.VkStructureType.VK_STRUCTURE_TYPE_APPLICATION_INFO
    pNext = null
    pApplicationName = "glfwvktest"
    applicationVersion = 1
    pEngineName = "glfwvktest"
    engineVersion = 1
    apiVersion =
        MAKE_VERSION 1 0 0

local dbgCreateInfo :  helpers.struct.VkDebugReportCallbackCreateInfoEXT
    sType = helpers.enum.VkStructureType.VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT
    pNext = null
    pfnCallback = helpers.extern.debugCallback
    pUserData = null
    flags =
        helpers.enum.VkDebugReportFlagBitsEXT.VK_DEBUG_REPORT_ERROR_BIT_EXT | helpers.enum.VkDebugReportFlagBitsEXT.VK_DEBUG_REPORT_WARNING_BIT_EXT

local instanceCreateInfo : helpers.struct.VkInstanceCreateInfo
    sType = helpers.enum.VkStructureType.VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO
    pNext = &dbgCreateInfo
    flags = 0
    pApplicationInfo = &appInfo
    enabledExtensionCount = instanceExtCount
    ppEnabledExtensionNames = reqInstanceExts

local instance : helpers.typedef.VkInstance

inline vkCheck (x)
    if (x != helpers.enum.VkResult.VK_SUCCESS)
        error "Vulkan failed"

vkCheck
    helpers.extern.vkCreateInstance &instanceCreateInfo null &instance
defer helpers.extern.vkDestroyInstance instance null

local gpuCount : u32
vkCheck
    helpers.extern.vkEnumeratePhysicalDevices instance &gpuCount null
local gpu : helpers.typedef.VkPhysicalDevice
let gpus =
    alloca-array helpers.typedef.VkPhysicalDevice gpuCount

vkCheck
    helpers.extern.vkEnumeratePhysicalDevices instance &gpuCount gpus

gpu = gpus @ 0

local deviceExtensionCount : u32 = 0
vkCheck
    helpers.extern.vkEnumerateDeviceExtensionProperties gpu null &deviceExtensionCount null

let enabledDeviceExtensions =
    alloca-array (pointer i8) 4

local enabledDeviceExtensionCount : u32 = 0
local swapchainExtFound = false;

do
    let deviceExtensions =
        alloca-array helpers.typedef.VkExtensionProperties deviceExtensionCount
    vkCheck
        helpers.extern.vkEnumerateDeviceExtensionProperties gpu null &deviceExtensionCount deviceExtensions

    for i in (range deviceExtensionCount)
        if ((string helpers.define.VK_KHR_SWAPCHAIN_EXTENSION_NAME) == (string (& (deviceExtensions @ i . extensionName @ 0))))
            swapchainExtFound = true
            enabledDeviceExtensions @ enabledDeviceExtensionCount = helpers.define.VK_KHR_SWAPCHAIN_EXTENSION_NAME
            enabledDeviceExtensionCount += 1

if (not swapchainExtFound)
    error "Need the swapchain extension, but it wasn't found"

local queueFamilyCount : u32
helpers.extern.vkGetPhysicalDeviceQueueFamilyProperties gpu &queueFamilyCount null
local suitableQueueFamilyIndex : u32

do
    let queueFamilyProperties =
        alloca-array helpers.typedef.VkQueueFamilyProperties queueFamilyCount
    helpers.extern.vkGetPhysicalDeviceQueueFamilyProperties gpu &queueFamilyCount queueFamilyProperties
    for i in (range queueFamilyCount)
        if ((queueFamilyProperties @ i . queueFlags) & helpers.enum.VkQueueFlagBits.VK_QUEUE_GRAPHICS_BIT)
            if (helpers.extern.glfwGetPhysicalDevicePresentationSupport instance gpu i)
                suitableQueueFamilyIndex = i
                break;

local queuePriority : f32 = 0.0

local queueCreateInfo : helpers.typedef.VkDeviceQueueCreateInfo
    sType = helpers.enum.VkStructureType.VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO
    pNext = null
    flags = 0
    queueFamilyIndex = suitableQueueFamilyIndex
    queueCount = 1
    pQueuePriorities = &queuePriority

local deviceCreateInfo : helpers.typedef.VkDeviceCreateInfo
    sType = helpers.enum.VkStructureType.VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO
    pNext = null
    flags = 0
    queueCreateInfoCount = 1
    pQueueCreateInfos = &queueCreateInfo
    enabledExtensionCount = enabledDeviceExtensionCount
    ppEnabledExtensionNames = enabledDeviceExtensions
    pEnabledFeatures = null

using helpers.extern

local device : helpers.typedef.VkDevice
vkCheck
    vkCreateDevice gpu &deviceCreateInfo null &device
defer vkDestroyDevice device null



local surface : helpers.typedef.VkSurfaceKHR
glfwCreateWindowSurface instance window null &surface
defer vkDestroySurfaceKHR instance surface null

local surfSupported : helpers.typedef.VkBool32 = helpers.define.VK_FALSE
vkCheck
    vkGetPhysicalDeviceSurfaceSupportKHR gpu suitableQueueFamilyIndex surface &surfSupported
assert
    surfSupported == helpers.define.VK_TRUE

using helpers.typedef

local queue : VkQueue
vkGetDeviceQueue device suitableQueueFamilyIndex 0 &queue

let surfCaps =
    alloca VkSurfaceCapabilitiesKHR
vkCheck
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR gpu surface surfCaps

let surfCaps = @surfCaps

local memProps : VkPhysicalDeviceMemoryProperties
vkGetPhysicalDeviceMemoryProperties gpu &memProps

local presentModeCount : u32
vkCheck
    vkGetPhysicalDeviceSurfacePresentModesKHR gpu surface &presentModeCount null
local swapchainPresentMode : VkPresentModeKHR = VkPresentModeKHR.VK_PRESENT_MODE_FIFO_KHR;

do
    local presentModes =
        alloca-array VkPresentModeKHR presentModeCount
    vkCheck
        vkGetPhysicalDeviceSurfacePresentModesKHR gpu surface &presentModeCount presentModes

    for i in (range presentModeCount)
        if (presentModes @ i == VkPresentModeKHR.VK_PRESENT_MODE_MAILBOX_KHR)
            swapchainPresentMode = VkPresentModeKHR.VK_PRESENT_MODE_MAILBOX_KHR
            break;
        if (swapchainPresentMode != VkPresentModeKHR.VK_PRESENT_MODE_MAILBOX_KHR and presentModes @ i == VkPresentModeKHR.VK_PRESENT_MODE_IMMEDIATE_KHR)
            swapchainPresentMode = VkPresentModeKHR.VK_PRESENT_MODE_IMMEDIATE_KHR

local swapchainExtent : VkExtent2D
if (surfCaps.currentExtent.width == 0xffffffff)
    swapchainExtent =
        typeinit
            keyed width height
else
    swapchainExtent = surfCaps.currentExtent;
    width = surfCaps.currentExtent.width;
    height = surfCaps.currentExtent.height;

define preTransform
    if (surfCaps.supportedTransforms & VkSurfaceTransformFlagBitsKHR.VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR)
        VkSurfaceTransformFlagBitsKHR.VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR
    else
        deref surfCaps.currentTransform

local formatCount : u32
vkCheck
    vkGetPhysicalDeviceSurfaceFormatsKHR gpu surface &formatCount null
let suitableFormat =
    alloca VkFormat
local suitableColorSpace =
    alloca VkColorSpaceKHR
do
    let surfFormats =
        alloca-array VkSurfaceFormatKHR formatCount
    vkCheck
        vkGetPhysicalDeviceSurfaceFormatsKHR gpu surface &formatCount surfFormats
    if ((formatCount == 1) and (surfFormats @ 0 . format == VkFormat.VK_FORMAT_UNDEFINED))
        @suitableFormat = VkFormat.VK_FORMAT_B8G8R8A8_UNORM
    else
        assert
            formatCount >= 1
        @suitableFormat = surfFormats @ 0 . format
    @suitableColorSpace = surfFormats @ 0 . colorSpace

local swapchainCreateInfo : VkSwapchainCreateInfoKHR
    sType = VkStructureType.VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR
    pNext = null
    flags = 0
    surface = surface
    minImageCount = 2
    imageFormat = @suitableFormat
    imageColorSpace = @suitableColorSpace
    imageExtent = swapchainExtent
    imageUsage = VkImageUsageFlagBits.VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT
    preTransform = preTransform
    compositeAlpha = VkCompositeAlphaFlagBitsKHR.VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR
    imageArrayLayers = 1
    imageSharingMode = VkSharingMode.VK_SHARING_MODE_EXCLUSIVE
    queueFamilyIndexCount = 0
    pQueueFamilyIndices = null
    presentMode = swapchainPresentMode
    oldSwapchain = null
    clipped = helpers.define.VK_TRUE

local swapchain : VkSwapchainKHR
vkCheck
    vkCreateSwapchainKHR device &swapchainCreateInfo null &swapchain
defer vkDestroySwapchainKHR device swapchain null


let swapchainImages =
    alloca-array VkImage 2
local swapchainImageCount : u32
vkCheck
    vkGetSwapchainImagesKHR device swapchain &swapchainImageCount null
assert
    swapchainImageCount == 2
vkCheck
    vkGetSwapchainImagesKHR device swapchain &swapchainImageCount swapchainImages

local currentSwapchainImage : u32 = 0

local cmdPoolCreateInfo : VkCommandPoolCreateInfo
    sType = VkStructureType.VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO
    pNext = null
    flags = 0
    queueFamilyIndex = suitableQueueFamilyIndex

local cmdPool : VkCommandPool
vkCheck
    vkCreateCommandPool device &cmdPoolCreateInfo null &cmdPool
defer vkDestroyCommandPool device cmdPool null

local depthFormat : VkFormat = VkFormat.VK_FORMAT_D32_SFLOAT_S8_UINT
local dsImageCreateInfo : VkImageCreateInfo
    sType = VkStructureType.VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO
    pNext = null
    flags = 0
    imageType = VkImageType.VK_IMAGE_TYPE_2D
    format = depthFormat
    extent =
        typeinit width height 1
    mipLevels = 1
    arrayLayers = 1
    samples = VkSampleCountFlagBits.VK_SAMPLE_COUNT_1_BIT
    tiling = VkImageTiling.VK_IMAGE_TILING_OPTIMAL
    usage = VkImageUsageFlagBits.VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT
    sharingMode = VkSharingMode.VK_SHARING_MODE_EXCLUSIVE
    initialLayout = VkImageLayout.VK_IMAGE_LAYOUT_UNDEFINED

local dsivCreateInfo : VkImageViewCreateInfo
    sType = VkStructureType.VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO
    pNext = null
    flags = 0
    image = null
    format = depthFormat
    components =
        typeinit
            VkComponentSwizzle.VK_COMPONENT_SWIZZLE_R
            VkComponentSwizzle.VK_COMPONENT_SWIZZLE_G
            VkComponentSwizzle.VK_COMPONENT_SWIZZLE_B
            VkComponentSwizzle.VK_COMPONENT_SWIZZLE_A
    subresourceRange =
        typeinit
            aspectMask = VkImageAspectFlagBits.VK_IMAGE_ASPECT_DEPTH_BIT
            baseMipLevel = 0
            levelCount = 1
            baseArrayLayer = 0
            layerCount = 1
    viewType = VkImageViewType.VK_IMAGE_VIEW_TYPE_2D


local dsMemReqs : VkMemoryRequirements
local dsImage : VkImage
vkCheck
    vkCreateImage device &dsImageCreateInfo null &dsImage
defer vkDestroyImage device dsImage null

vkGetImageMemoryRequirements device dsImage &dsMemReqs
local dsMemAllocInfo : VkMemoryAllocateInfo
    sType = VkStructureType.VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO
    pNext = null
    allocationSize = dsMemReqs.size
    memoryTypeIndex = 0

assert
    getMemoryTypeFromProperties memProps dsMemReqs.memoryTypeBits 0 &dsMemAllocInfo.memoryTypeIndex

local dsMemory : VkDeviceMemory
vkCheck
    vkAllocateMemory device &dsMemAllocInfo null &dsMemory
vkCheck
    vkBindImageMemory device dsImage dsMemory 0
defer vkFreeMemory device dsMemory null

local cmdBuf =
    nullof VkCommandBuffer
setImageLayout &cmdBuf device cmdPool dsImage (VkImageAspectFlagBits.VK_IMAGE_ASPECT_DEPTH_BIT | VkImageAspectFlagBits.VK_IMAGE_ASPECT_STENCIL_BIT) VkImageLayout.VK_IMAGE_LAYOUT_UNDEFINED VkImageLayout.VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL (bitcast 0 VkAccessFlagBits)

local dsImageView : VkImageView
dsivCreateInfo.image = dsImage;
vkCheck
    vkCreateImageView device &dsivCreateInfo null &dsImageView
defer vkDestroyImageView device dsImageView null


local ivCreateInfo : VkImageViewCreateInfo
    sType = VkStructureType.VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO
    pNext = null
    flags = 0
    format = @suitableFormat
    components =
        typeinit
            VkComponentSwizzle.VK_COMPONENT_SWIZZLE_R
            VkComponentSwizzle.VK_COMPONENT_SWIZZLE_G
            VkComponentSwizzle.VK_COMPONENT_SWIZZLE_B
            VkComponentSwizzle.VK_COMPONENT_SWIZZLE_A
    subresourceRange =
        typeinit
            aspectMask = VkImageAspectFlagBits.VK_IMAGE_ASPECT_COLOR_BIT
            baseMipLevel = 0
            levelCount = 1
            baseArrayLayer = 0
            layerCount = 1
    viewType = VkImageViewType.VK_IMAGE_VIEW_TYPE_2D

let presentViews =
    alloca-array VkImageView 2

for i in (range 2)
    ivCreateInfo.image = swapchainImages @ i
    vkCheck
        vkCreateImageView device &ivCreateInfo null
            & (presentViews @ i)

local attachments =
    arrayof VkAttachmentDescription
        typeinit
            format = @suitableFormat
            samples = VkSampleCountFlagBits.VK_SAMPLE_COUNT_1_BIT
            loadOp = VkAttachmentLoadOp.VK_ATTACHMENT_LOAD_OP_CLEAR
            storeOp = VkAttachmentStoreOp.VK_ATTACHMENT_STORE_OP_STORE
            stencilLoadOp = VkAttachmentLoadOp.VK_ATTACHMENT_LOAD_OP_DONT_CARE
            stencilStoreOp = VkAttachmentStoreOp.VK_ATTACHMENT_STORE_OP_DONT_CARE
            initialLayout = VkImageLayout.VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
            finalLayout = VkImageLayout.VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
        typeinit
            format = depthFormat
            samples = VkSampleCountFlagBits.VK_SAMPLE_COUNT_1_BIT
            loadOp = VkAttachmentLoadOp.VK_ATTACHMENT_LOAD_OP_CLEAR
            storeOp = VkAttachmentStoreOp.VK_ATTACHMENT_STORE_OP_DONT_CARE
            stencilLoadOp = VkAttachmentLoadOp.VK_ATTACHMENT_LOAD_OP_DONT_CARE
            stencilStoreOp = VkAttachmentStoreOp.VK_ATTACHMENT_STORE_OP_DONT_CARE
            initialLayout = VkImageLayout.VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
            finalLayout = VkImageLayout.VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL

local colorReference : VkAttachmentReference
    attachment = 0
    layout = VkImageLayout.VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL

local depthStencilReference : VkAttachmentReference
    attachment = 1
    layout = VkImageLayout.VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL

local subpassDesc : VkSubpassDescription
    pipelineBindPoint = VkPipelineBindPoint.VK_PIPELINE_BIND_POINT_GRAPHICS
    flags = 0
    inputAttachmentCount = 0
    pInputAttachments = null
    colorAttachmentCount = 1
    pColorAttachments = &colorReference
    pResolveAttachments = null
    pDepthStencilAttachment = &depthStencilReference
    preserveAttachmentCount = 0
    pPreserveAttachments = null

local rpCreateInfo : VkRenderPassCreateInfo
    sType = VkStructureType.VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO
    pNext = null
    flags = 0
    attachmentCount = 2
    pAttachments = attachments
    subpassCount = 1
    pSubpasses = &subpassDesc
    dependencyCount = 0
    pDependencies = null

local renderPass : VkRenderPass
vkCheck
    vkCreateRenderPass device &rpCreateInfo null &renderPass
defer vkDestroyRenderPass device renderPass null


let fbAttachments =
    alloca-array VkImageView 2

fbAttachments @ 1 = dsImageView

local fbCreateInfo : VkFramebufferCreateInfo
    sType = VkStructureType.VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO
    pNext = null
    flags = 0
    renderPass = renderPass
    attachmentCount = 2
    pAttachments = fbAttachments
    layers = 1
    keyed width height

local framebuffers =
    alloca-array VkFramebuffer 2
for i in (range 2)
    fbAttachments @ 0 = presentViews @ i
    vkCheck
        vkCreateFramebuffer device &fbCreateInfo null
            & (framebuffers @ i)

vkCheck
    vkEndCommandBuffer cmdBuf
local submitInfo : VkSubmitInfo
    sType = VkStructureType.VK_STRUCTURE_TYPE_SUBMIT_INFO
    pNext = null
    waitSemaphoreCount = 0
    pWaitSemaphores = null
    pWaitDstStageMask = null
    commandBufferCount = 1
    pCommandBuffers = &cmdBuf
    signalSemaphoreCount = 0
    pSignalSemaphores = null

vkCheck
    vkQueueSubmit queue 1 &submitInfo null
vkCheck
    vkQueueWaitIdle queue

vkFreeCommandBuffers device cmdPool 1 &cmdBuf
cmdBuf = null


local cmdBufAllocInfo : VkCommandBufferAllocateInfo
    sType = VkStructureType.VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO
    pNext = null
    commandPool = cmdPool
    level = VkCommandBufferLevel.VK_COMMAND_BUFFER_LEVEL_PRIMARY
    commandBufferCount = 2

let cmdBufs =
    alloca-array VkCommandBuffer 2
vkCheck
    vkAllocateCommandBuffers device &cmdBufAllocInfo cmdBufs

local cmdBufInhInfo : VkCommandBufferInheritanceInfo
    sType = VkStructureType.VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO
    pNext = null
    renderPass = null
    subpass = 0
    framebuffer = null
    occlusionQueryEnable = helpers.define.VK_FALSE
    queryFlags = 0
    pipelineStatistics = 0

local cmdBufBeginInfo : VkCommandBufferBeginInfo
    sType = VkStructureType.VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO
    pNext = null
    flags = 0
    pInheritanceInfo = &cmdBufInhInfo

local clearValues : (array VkClearValue 2) =
    arrayof VkClearValue
        typeinit
            color =
                typeinit
                    float32 =
                        arrayof f32 0.5 0.5 0.5 1.0
        typeinit
            depthStencil =
                typeinit 1.0 0

helpers.extern.execute &width &height window device queue swapchain cmdPool swapchainImages currentSwapchainImage cmdBuf presentViews renderPass framebuffers cmdBufs &cmdBufBeginInfo &clearValues

